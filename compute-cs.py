#!/usr/bin/python3

servers = int(input("s = "))
clients = int(input("c = "))

memo = [[None for c in range(clients+1)] for s in range(servers+1)]
for c in range(clients+1):
    memo[0][c] = 1
for s in range(servers+1):
    memo[s][0] = 1

for s in range(1, servers+1):
    for c in range(1, clients+1):
        memo[s][c] = c * memo[s-1][c-1] + memo[s-1][c]

print(f"n = {memo[servers][clients]}")
