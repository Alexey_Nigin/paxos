--------------------------------- MODULE cs ---------------------------------

CONSTANT Client, Server

VARIABLE link, semaphore

TypeOK ==
  /\ link \subseteq (Client \X Server)
  /\ semaphore \subseteq Server

Init ==
  /\ semaphore = Server
  /\ link = {}

Connect(c, s) ==
  /\ s \in semaphore
  /\ link' = link \union {<<c, s>>}
  /\ semaphore' = semaphore \ {s}

Disconnect(c, s) ==
  /\ <<c, s>> \in link
  /\ link' = link \ {<<c, s>>}
  /\ semaphore' = semaphore \union {s}

Next == \E c \in Client, s \in Server : Connect(c, s) \/ Disconnect(c, s)

Spec == Init /\ [][Next]_<<link, semaphore>>

=============================================================================
