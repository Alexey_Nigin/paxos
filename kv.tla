--------------------------------- MODULE kv ---------------------------------

CONSTANT Node, Key, Value

VARIABLE table, owner, transfer_msg

TypeOK ==
  /\ table \subseteq (Node \X Key \X Value)
  /\ owner \subseteq (Node \X Key)
  /\ transfer_msg \subseteq (Node \X Key \X Value)

Init ==
  /\ table = {}
  /\ transfer_msg = {}
  /\ owner \in {o \in SUBSET (Node \X Key) :
             (\A o1, o2 \in o : (o1[2] = o2[2]) => (o1[1] = o2[1]))}

Reshard(n1, n2, k, v) ==
  /\ <<n1, k, v>> \in table
  /\ table' = table \ {<<n1, k, v>>}
  /\ owner' = owner \ {<<n1, k>>}
  /\ transfer_msg' = transfer_msg \union {<<n2, k, v>>}

Recv_Transfer_Msg(n, k, v) ==
  /\ <<n, k, v>> \in transfer_msg
  /\ transfer_msg' = transfer_msg \ {<<n, k, v>>}
  /\ table' = table \union {<<n, k, v>>}
  /\ owner' = owner \union {<<n, k>>}

Put(n, k, v) ==
  /\ <<n, k>> \in owner
  /\ table' = (table \ {<<n, k, v_all>> : v_all \in Value})
             \union {<<n, k, v>>}
  /\ UNCHANGED <<owner, transfer_msg>>

Next ==
  \/ \E n1, n2 \in Node, k \in Key, v \in Value : Reshard(n1, n2, k, v)
  \/ \E n \in Node, k \in Key, v \in Value : Recv_Transfer_Msg(n, k, v)
  \/ \E n \in Node, k \in Key, v \in Value : Put(n, k, v)

Spec == Init /\ [][Next]_<<table, owner, transfer_msg>>

=============================================================================
