#lang ivy1.7

type acceptor
type value
type quorum
type ballot

individual negone:ballot
relation le(X:ballot, Y:ballot)

# A total order helper module
module total_order(r) = {
    axiom r(X, X)                        # Reflexivity
    axiom r(X, Y) & r(Y, Z) -> r(X, Z)  # Transitivity
    axiom r(X, Y) & r(Y, X) -> X = Y    # Anti-symmetry
    axiom r(X, Y) | r(Y, X)             # Totality
}

# ballots are totally ordered with a least element called zero
instantiate total_order(le)

relation member(A: acceptor, Q: quorum)
axiom forall Q1, Q2. exists A:acceptor. member(A, Q1) & member(A, Q2)

relation msg_1a(B: ballot)							# ballot B started
relation msg_1b(A: acceptor, B: ballot)				# A joined ballot B                      # modified to avoid acceptor -> ballot arc and keep (Spec + Invariant) decidable # mbal and mval not really important and updated only once in phase_2b, modelled in a different manner
relation msg_2a(B: ballot, V: value)				# V is proposed in ballot B
relation msg_2b(A: acceptor, B: ballot, V: value)	# A voted V in ballot B

relation left_ballot(A: acceptor, B: ballot)        # (maxBal(A) > B) == left_ballot(A, B)   # introduced to avoid acceptor -> ballot arc and keep (Spec + Invariant) decidable
#function maxbal(A: acceptor): ballot

relation decision(B: ballot, V: value)              # added to easily model safety property, denotes the final value decided in a ballot

# function decision_quorum(B: ballot, V: value): quorum

relation choosable(B: ballot, V: value, Q: quorum)  # temporarily added for additional experimentation
# axiom forall B, V, Q. choosable(B, V, Q) = ((forall A. (member(A, Q) -> (~left_ballot(A, B) | msg_2b(A, B, V)))))

################################################################################
#
# Protocol description
#
################################################################################

after init {
    msg_1a(A) := false;
    msg_1b(A, B) := false;
    msg_2a(B, V) := false;
    msg_2b(A, B, V) := false;

    left_ballot(A, B) := false;
#    maxbal(A) := negone;

    decision(B, V) := false;

#    choosable(B, V, Q) := ((forall A. (member(A, Q) -> (~left_ballot(A, B) | msg_2b(A, B, V)))));
}

action phase_1a(b: ballot) = {
    require b ~= negone;
    
    msg_1a(b) := true;

#    choosable(B, V, Q) := ((forall A. (member(A, Q) -> (~left_ballot(A, B) | msg_2b(A, B, V)))));
}

action phase_1b(a: acceptor, b: ballot) = {
    require b ~= negone;
    require msg_1a(b);
    require ~left_ballot(a, b);
#    require ~le(b, maxbal(a));
    
    msg_1b(a, b) := true;
    left_ballot(a, B) := left_ballot(a, B) | ~le(b, B);
#    maxbal(a) := b;

#    choosable(B, V, Q) := ((forall A. (member(A, Q) -> (~left_ballot(A, B) | msg_2b(A, B, V)))));
}

action phase_2a(b: ballot, v: value, q: quorum, maxb: ballot) = {
    require b ~= negone;
    require (forall V. ~msg_2a(b, V));
    require (forall A. member(A, q) -> msg_1b(A, b));
#    require ( (maxb = negone & 
#              (forall A,MAXB,V. ~(member(A, q) & ~le(b,MAXB) & msg_2b(A,MAXB,V)))) |
#              (maxb ~= negone & 
#              (exists A. member(A, q) & ~le(b,maxb) & msg_2b(A,maxb,v)) &
#              (forall A,MAXB,V. (member(A, q) & ~le(b,MAXB) & msg_2b(A,MAXB,V)) -> le(MAXB,maxb))));

#    require ( (forall A. member(A, q) & msg_1b(A, b) -> (forall MBAL, V. ~msg_2b(A, MBAL, V))) |
#              (exists A, MBAL. member(A, q) & msg_1b(A, b) & msg_2b(A, MBAL, v) &
#              (forall A2, MBAL2, V2. member(A2, q) & msg_1b(A2, b) & msg_2b(A2, MBAL2, V2) -> le(MBAL2, MBAL))));

    require ( (forall A. member(A, q) & msg_1b(A, b) -> (forall MBAL2, V2. ~msg_2b(A, MBAL2, V2))) |
              (exists A. member(A, q) & msg_1b(A, b) & msg_2b(A, maxb, v) &
              (forall A2, MBAL2, V2. member(A2, q) & msg_1b(A2, b) & msg_2b(A2, MBAL2, V2) -> le(MBAL2, maxb))));

    msg_2a(b, v) := true;

#    choosable(B, V, Q) := ((forall A. (member(A, Q) -> (~left_ballot(A, B) | msg_2b(A, B, V)))));
}

action phase_2b(a: acceptor, b: ballot, v: value) = {
    require b ~= negone;
    require msg_2a(b, v);
    require ~left_ballot(a, b);
#    require le(maxbal(a), b);

    msg_2b(a, b, v) := true;

#    choosable(B, V, Q) := ((forall A. (member(A, Q) -> (~left_ballot(A, B) | msg_2b(A, B, V)))));
}

action decide(b: ballot, v: value, q: quorum) = {
    require b ~= negone;
    require (forall A. member(A, q) -> msg_2b(A, b, v));

    decision(b, v) := true;
#    decision_quorum(b, v) := q;

#    choosable(B, V, Q) := ((forall A. (member(A, Q) -> (~left_ballot(A, B) | msg_2b(A, B, V)))));
}

export phase_1a
export phase_1b
export phase_2a
export phase_2b
export decide

# the safety property
invariant [safety] decision(B1, V1) & decision(B2, V2) -> V1 = V2

invariant [help_1_unique_2a] msg_2a(B, V1) & msg_2a(B, V2) -> V1 = V2
invariant [help_2_2b_2a] msg_2b(A, B, V) -> msg_2a(B, V)
invariant [help_3_1b_left] msg_1b(A, B2) & ~le(B2, B1) -> left_ballot(A, B1)
#invariant [help_3_1b_left] msg_1b(A, B2) & ~le(B2, B1) -> ~le(maxbal(A), B1)

invariant [help_4_decision]
  forall B, V.
    decision(B, V) ->
    exists Q.
      forall A. member(A, Q) -> msg_2b(A, B, V)

#invariant [help_5_choosable]
#   forall B1, V1, B2, V2.
#     (exists Q. choosable(B1, V1, Q)) &
#     ~le(B2, B1) &
#     msg_2a(B2, V2) ->
#     V1 = V2

#invariant [help_5_choosable_def] forall B, V, Q.
#  choosable(B, V, Q) <->
#  (forall A. member(A, Q) -> ~left_ballot(A, B) | msg_2b(A, B, V))

invariant [help_5_hard]
   forall B1, V1, B2, V2.
     (exists Q. forall A. member(A, Q) -> ~left_ballot(A, B1) | msg_2b(A, B1, V1)) &
     ~le(B2, B1) &
     msg_2a(B2, V2) ->
     V1 = V2
#invariant [help_5_hard]
#   forall B1, V1, B2, V2.
#     (exists Q. forall A. member(A, Q) -> le(maxbal(A), B1) | msg_2b(A, B1, V1)) &
#     ~le(B2, B1) &
#     msg_2a(B2, V2) ->
#    V1 = V2

#invariant [help_6_negone] ~msg_2b(B, negone, V)

#invariant [help_4_decision2]
#  forall B, V.
#    decision(B, V) ->
#    forall A. member(A, decision_quorum(B, V)) -> msg_2b(A, B, V)

