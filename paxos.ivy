#lang ivy1.7

# Modules

module total_order(r) = {
    axiom r(X, X)                       # Reflexivity
    axiom r(X, Y) & r(Y, Z) -> r(X, Z)  # Transitivity
    axiom r(X, Y) & r(Y, X) -> X = Y    # Anti-symmetry
    axiom r(X, Y) | r(Y, X)             # Totality
}

# Types

# `value` is an unordered set, containing all valid values as well as `none`
# In the TLA+ spec, None is defined as something that is not a ballot number. It
# always bothered me, because None is clearly supposed to be a value, not a
# ballot. In fact, ballots just use -1 and definitely don't need another None
# apart from that. Maybe it's a mistake in Lamport's spec? I don't know, but in
# any case, since None is compared to other values, in the Ivy spec `none` has
# to be of type `value`. This is slightly different from Lamport's spec, where
# Value is the set of valid values only, and this difference might affect
# something down the line, so be on a lookout for that.
type value
individual none:value

# `acceptor` is an unordered set of all acceptors
type acceptor

# `quorum` is the set of all quorums, satisfying the quorum assumption
type quorum
relation member(A:acceptor, Q:quorum)
axiom forall Q1:quorum, Q2:quorum . exists A:acceptor . member(A, Q1) & member(A, Q2)

# `ballot` is a totally ordered set, containing all ballots as well as `negone`
# In the TLA+ spec, Ballot is the set of all natural numbers, and -1 is not part
# of Ballot. I had to change it for the same reason as with values, so be on a
# lookout for that as well.
type ballot
relation le(X:ballot, Y:ballot)
instantiate total_order(le)
individual negone:ballot
axiom le(negone, X)

# State

# These things are the states of individual acceptors
function maxbal(A:acceptor) : ballot
function maxvbal(A:acceptor) : ballot
function maxval(A:acceptor) : value

# Here, true means that the corresponding message is floating in the network
# In the TLA+ spec, messages of all four types are thrown into the same set, but
# I split them to make everything easier. This split shouldn't cause any
# problems.
relation message_1a(B:ballot)
relation message_1b(A:acceptor, B:ballot, MBAL:ballot, MVAL:value)
relation message_2a(B:ballot, V:value)
relation message_2b(A:acceptor, B:ballot, V:value)

# Behavior

after init {
    maxbal(A) := negone;
    maxvbal(A) := negone;
    maxval(A) := none;
    message_1a(B) := false;
    message_1b(A, B, MBAL, MVAL) := false;
    message_2a(B, V) := false;
    message_2b(A, B, V) := false;
}

action phase_1a(b:ballot) = {
    require b ~= negone;
    message_1a(b) := true;
}

action phase_1b(a:acceptor) = {
    if some b:ballot . b ~= negone & message_1a(b) & ~le(b, maxbal(a)) {
        maxbal(a) := b;
        message_1b(a, b, maxvbal(a), maxval(a)) := true;
    }
}

# True iff set Q1b constructed from _q_ and _b_ has message with acceptor a
relation q1b(a:acceptor, _q_:quorum, _b_:ballot) = exists MBAL:ballot, MVAL:value . message_1b(a, _b_, MBAL, MVAL) & member(a, _q_)

# True iff set Q1bv constructed from _q_ and _b_ has message with <mbal, mval>
relation q1bv(mbal:ballot, mval:value, _q_:quorum, _b_:ballot) = exists A:acceptor . message_1b(A, _b_, mbal, mval) & member(A, _q_) & mbal ~= negone

# This is the good old ShowsSafeAt from the TLA+ spec
# It's quite an absolute unit, there should be a better way to write it.
relation shows_safe_at(q:quorum, b:ballot, v:value) = (forall A:acceptor . member(A, q) -> q1b(A, q, b)) & ((forall MBAL:ballot, MVAL:value . ~q1bv(MBAL, MVAL, q, b)) | (exists MBAL:ballot . q1bv(MBAL, v, q, b) & forall MMBAL:ballot, MMVAL:value . q1bv(MMBAL, MMVAL, q, b) -> le(MMBAL, MBAL)))

action phase_2a(b:ballot, v:value) = {
    require b ~= negone & v ~= none;
    require ~message_2a(b, V);
    require exists Q:quorum . shows_safe_at(Q, b, v);
    message_2a(b, v) := true;
}

action phase_2b(a:acceptor) = {
    if some b:ballot, v:value . b ~= negone & v ~= none & message_2a(b, v) & le(maxbal(a), b) {
        maxbal(a) := b;
        maxvbal(a) := b;
        maxval(a) := v;
        message_2b(a, b, v) := true;
    }
}

export phase_1a
export phase_1b
export phase_2a
export phase_2b

# Invariants

# The correctness invariant
# If values V1 and V2 are chosen, they must be equal.
invariant (V1 ~= none & V2 ~= none & exists Q1:quorum, Q2:quorum, B1:ballot, B2:ballot . forall A:acceptor . (member(A, Q1) -> message_2b(A, B1, V1)) & (member(A, Q2) -> message_2b(A, B2, V2))) -> (V1 = V2)

# These invariants check that messages only have `negone`/`none` in right places
invariant ~message_1a(negone)
invariant ~message_1b(A, negone, MBAL, MVAL)
invariant ~message_2a(negone, V) & ~message_2a(B, none)
invariant ~message_2b(A, negone, V) & ~message_2b(A, B, none)
