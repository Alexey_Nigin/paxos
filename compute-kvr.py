#!/usr/bin/python3

states = 0

nodes   = int(input("n = "))
keys    = int(input("k = "))
values  = int(input("v = "))
seqnums = int(input("s = "))

slots = [[True for s in range(seqnums)] for n in range(nodes)]
chains = [[] for k in range(keys)]
canons_incomplete = set()
canons_complete = set()

def canonize_incomplete(key, chain):
    canon = []
    for l in range(len(chain)-1):
        canon.append((chain[l][0], chain[l][1], chain[l+1][0]))
    canon.sort()
    canon.insert(0, key)
    canon.append((chain[-1][0], chain[-1][1]))
    canon = ' '.join(map(str, canon))
    return canon

def canonize_complete(key, chain, node):
    canon = []
    for l in range(len(chain)-1):
        canon.append((chain[l][0], chain[l][1], chain[l+1][0]))
    canon.append((chain[-1][0], chain[-1][1], node))
    canon.sort()
    canon.insert(0, key)
    canon.append(node)
    canon = ' '.join(map(str, canon))
    return canon

def complicate():
    global states, nodes, values, chains, canons_incomplete, canons_complete
    print(chains)
    new_states = 1
    for k in range(keys):
        c = chains[k]
        if len(c) == 0:
            new_states *= nodes * (values + 1) + 1
            print(nodes * (values + 1) + 1)
        else:
            incomplete = 0
            canon_incomplete = canonize_incomplete(k, c)
            if not canon_incomplete in canons_incomplete:
                canons_incomplete.add(canon_incomplete)
                incomplete = (6 * values + 2) ** (len(c) - 1) * 2 * values
                incomplete *= nodes
            complete = 0
            for n in range(nodes):
                canon_complete = canonize_complete(k, c, n)
                if not canon_complete in canons_complete:
                    canons_complete.add(canon_complete)
                    complete += (6 * values + 2) ** len(c) * values
            print(incomplete, complete)
            new_states *= incomplete + complete
    states += new_states

def partition():
    global nodes, keys, seqnums, slots, chains
    complicate()
    for n in range(nodes):
        for s in range(seqnums):
            if not slots[n][s]:
                continue
            slots[n][s] = False
            for k in range(keys):
                chains[k].append((n, s))
                partition()
                chains[k].pop()
            slots[n][s] = True

partition()

print(states)
