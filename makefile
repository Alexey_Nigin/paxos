kv.min: kv.py
	./kv.py
	../bin/espresso -D exact -o eqntott kv.pla | ./pp.sh > kv.min

clean:
	rm -f *.pla *.min

.PHONY: clean
