# Number of reachable states in Client-Server

Let $`N(s,c)`$ be the number of reachable states in an instance of Client-Server
with $`s`$ servers and $`c`$ clients.

When there are no servers or when there are no clients, the initial state is the
only reachable state, so

```math
N(0,c) = N(s,0) = 1
```

When there are $`s \geq 1`$ servers and $`c \geq 1`$ clients, let's fix one
server. This server is either connected to a client, or not.

If it is connected to a client, it can be connected to any of the $`c`$ clients,
and the remaining $`s-1`$ servers have $`c-1`$ clients left to interact with.
$`s-1`$ servers can interact with $`c-1`$ clients in $`N(s-1,c-1)`$ ways, and we
need to multiply this by the number of clients that our fixed server can connect
to, giving us $`c \cdot N(s-1,c-1)`$ states. If the fixed server is not
connected to a client, then it leaves all $`c`$ clients for the remaining
$`s-1`$ servers, giving us $`N(s-1,c)`$ states. Summing up the numbers of states
for those possibilities, we get the following recurrence relation:

```math
N(s,c) = c \cdot N(s-1,c-1) + N(s-1,c)
```

At first glance these look like Stirling numbers, but they're not, and I don't
think there is a clean closed form for this recurrence. But we don't really need
a closed form, I wrote a tiny Python script that can compute this recurrence for
any reasonable $`s`$ and $`c`$.
